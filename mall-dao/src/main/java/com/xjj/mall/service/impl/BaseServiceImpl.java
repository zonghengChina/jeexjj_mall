/****************************************************
 * Description: ServiceImpl for t_mall_base
 * Copyright:   Copyright (c) 2018
 * Company:     xjj
 * @author      zhanghejie
 * @version     1.0
 * @see
	HISTORY
    *  2018-09-13 zhanghejie Create File
**************************************************/

package com.xjj.mall.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.xjj.framework.dao.XjjDAO;
import com.xjj.framework.service.XjjServiceSupport;
import com.xjj.mall.entity.BaseEntity;
import com.xjj.mall.dao.BaseDao;
import com.xjj.mall.service.BaseService;

@Service
public class BaseServiceImpl extends XjjServiceSupport<BaseEntity> implements BaseService {

	@Autowired
	private BaseDao baseDao;

	@Override
	public XjjDAO<BaseEntity> getDao() {
		
		return baseDao;
	}
}