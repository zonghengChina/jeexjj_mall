<#--
/****************************************************
 * Description: t_mall_permission的输入页面，包括添加和修改
 * Copyright:   Copyright (c) 2018
 * Company:     xjj
 * @author      zhanghejie
 * @version     1.0
 * @see
	HISTORY
    *  2018-09-13 zhanghejie Create File
**************************************************/
-->
<#include "/templates/xjj-index.ftl"> 

<@input url="${base}/mall/permission/save" id=tabId>
   <input type="hidden" name="id" value="${permission.id}"/>
   
   <@formgroup title='name'>
	<input type="text" name="name" value="${permission.name}" >
   </@formgroup>
   <@formgroup title='permission'>
	<input type="text" name="permission" value="${permission.permission}" >
   </@formgroup>
</@input>